import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit


class LineEditTest(QWidget):
    """
    """
    def __init__(self):
        super(LineEditTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("校验器");

        layout=QFormLayout();
        self.setLayout(layout)

        edit1=QLineEdit();
        edit1.setValidator(QIntValidator());
        edit1.setMaxLength(4);
        edit1.setAlignment(Qt.AlignRight);
        edit1.setFont(QFont('Arial',20))

        edit2 = QLineEdit();
        edit2.setValidator(QDoubleValidator(0.99,9.99,2))

        edit3=QLineEdit();
        edit3.setInputMask('99_9999_999999;#')

        edit4=QLineEdit();
        edit4.textChanged.connect(self.textChange);
        edit4.editingFinished.connect(self.textFinish);
        layout.addRow("整数", edit1);
        layout.addRow("double", edit2);
        layout.addRow("mask", edit3);
        layout.addRow("文本变化", edit4);

    def textChange(self,text):
        print(text)
    def textFinish(self):
        print('文本结束输入')


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = LineEditTest();
    main.show();
    sys.exit(app.exec_());
