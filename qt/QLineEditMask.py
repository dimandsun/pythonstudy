import sys

from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit


class LineEditMask(QWidget):
    """
A   ASCII字母字符是必须输入的(A-Z、a-z)
a   ASCII字母字符是允许输入的,但不是必需的(A-Z、a-z)
N   ASCII字母字符是必须输入的(A-Z、a-z、0-9)
n   ASCII字母字符是允许输入的,但不是必需的(A-Z、a-z、0-9)
X   任何字符都是必须输入的
x   任何字符都是允许输入的，但不是必需的
9   ASCII数字字符是必须输入的(O-9)
0   ASCII数字字符是允许输入的，但不是必需的(0-9)
D   ASCII数字字符是必须输入的(1-9)
d   ASCII数字字符是允许输入的,但不是必需的(1-9)
#   ASCII数字字符或加减符号是允许输入的，但不是必需的
H   十六进制格式字符是必须输入的(A-F、a-f、0-9)
h   十六进制格式字符是允许输入的,但不是必需的(A-F、a-f、0-9)
B   进制格式字符是必须输入的(0,1)
b   二进制格式字符是允许输入的,但不是必需的(0,1)
>   所有的字母字符都大写
<   所有的字母字符都小写
!   关闭大小写转换
\   使用"\"转义上面列出的字符

    """

    def __init__(self):
        super(LineEditMask, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("掩码");

        layout = QFormLayout();
        lineIp = QLineEdit();
        linemac = QLineEdit();
        linedate = QLineEdit();
        linelicence = QLineEdit();

        layout.addRow("ip", lineIp);
        layout.addRow("mac", linemac);
        layout.addRow("日期", linedate);
        layout.addRow("许可证", linelicence);

        lineIp.setInputMask("000.000.000.000;_");
        linemac.setInputMask("HH:HH:HH:HH:HH:HH;_");
        linedate.setInputMask("0000-00-00");
        linelicence.setInputMask(">AAAAA-AAAAA-AAAAA-AAAAA-AAAAA;#");

        self.setLayout(layout)


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = LineEditMask();
    main.show();
    sys.exit(app.exec_());
