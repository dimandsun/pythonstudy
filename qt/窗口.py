import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QApplication, QToolTip


# 窗口类型
# 主窗口 QMainWindow 包含菜单栏、状态栏、标题栏
# 对话窗口 QDialog
# 不确定窗口 QWidget

class FirstMainWindow(QMainWindow):
    def __init__(self):
        super(FirstMainWindow, self).__init__();
        # 设置主窗口标题
        self.setWindowTitle("主窗口标题");
        # 设置窗口尺寸
        self.resize(300, 300);
        # 窗口移动
        self.move(100,200);
        # 状态栏
        self.status = self.statusBar();
        # 状态栏上显示只存在5秒的信息
        self.status.showMessage('只存在5秒的消息', 5000);
        # 设置窗口位置和尺寸，参数1和2为位置，参数2和3为尺寸
        self.setGeometry(300,700,250,350);
        # 设置窗口图标，只在windows系统下生效
        # self.setWindowIcon(QIcon('../images/cat.ico'));



if __name__ == '__main__':
    app = QApplication(sys.argv);
    # 设置主窗口图标和应用程序图标
    app.setWindowIcon(QIcon('../images/cat.ico'));
    main = FirstMainWindow();
    main.show();
    sys.exit(app.exec_());
