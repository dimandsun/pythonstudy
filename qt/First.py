import sys

from PyQt5.QtWidgets import QApplication, QWidget

# Q


if __name__=='__main__':
    # 创建QApplication实例
    app=QApplication(sys.argv);
    # 创建窗口
    widow=QWidget();
    # 设置窗口尺寸
    widow.resize(300,150)
    # 移动窗口
    widow.move(300,300);
    # 设置窗口标题
    widow.setWindowTitle("窗口标题");
    # 显示窗口
    widow.show();
    # 进入程序主循环，并通过exit确保安全结束
    sys.exit(app.exec_());

    # pip install Pyqt5Designer
    # 项目目录:$ProjectFileDir$，工作目录:$FileDir$
    # C:\Users\Samsung\AppData\Local\Programs\Python\Python312\Scripts 找到Pyqt5Designer和pyuic5程序
    # 实参 $FileName$ -o $FileNameWithoutExtension$.py
    # 根据ui文件生成py文件
    # 方法1 控制台到目录E:\wo\python\game\snake\doc下，执行 python -m PyQt5.uic.pyuic demo.ui -o demo.py
    # 方法2 控制台到目录E:\wo\python\game\snake\doc下，执行pyuic5 demo.ui -o demo.py
    # 方法3 外部工具pyuic