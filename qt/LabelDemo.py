import sys

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QPalette
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout


class ToolTipLabel(QWidget):
    def __init__(self):
        super().__init__();
        self.initUI();

    def initUI(self):
        label1 = QLabel();
        label2 = QLabel();
        label3 = QLabel();
        label4 = QLabel();
        label1.setText("<font color=yellow>标签1</font>");
        # 自动填充背景
        label1.setAutoFillBackground(True);
        # 调色板
        palette = QPalette();
        # 背景色为灰色,注意是QPalette.Window，而不是QPalette.window
        palette.setColor(QPalette.Window, Qt.gray)
        # 设置文本颜色为黑色
        palette.setColor(QPalette.Text, Qt.black)
        # 设置按钮的背景色为蓝色
        palette.setColor(QPalette.Button, Qt.blue)
        # label应用该调色板
        label1.setPalette(palette);
        # 居中对齐
        label1.setAlignment(Qt.AlignCenter);

        label2.setText("<a href='#'>标签2</a>")

        label3.setAlignment(Qt.AlignCenter);
        label3.setToolTip("图片标签");
        label3.setPixmap(QPixmap("../images/cat.ico"));

        label4.setText("<a href='https://www.baidu.com/'>百度一下</a>")
        label4.setAlignment(Qt.AlignRight);
        label4.setToolTip("超链接");
        vbox = QVBoxLayout();
        vbox.addWidget(label1);
        vbox.addWidget(label2);
        vbox.addWidget(label3);
        vbox.addWidget(label4);

        # 鼠标滑过事件，不能用label2.linkHovered(self.linkHovered());
        label2.linkHovered.connect(self.linkHovered);
        # 单击事件与打开超链接事件互斥
        label4.linkActivated.connect(self.linkClicked);
        label4.setOpenExternalLinks(True);

        self.setLayout(vbox);
        self.setWindowTitle("QLabel控件演示");

    def linkHovered(self):
        print('鼠标滑过');

    def linkClicked(self):
        print('单击');


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = ToolTipLabel();
    main.show();
    sys.exit(app.exec_());
