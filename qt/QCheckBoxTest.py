import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont, QIcon
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit, QDialog, QVBoxLayout, QPushButton, \
    QRadioButton, QHBoxLayout, QCheckBox


class CheckBoxTest(QWidget):
    """
    复选框三种状态
    未选中 0
    半选中 1
    选中 2
    """

    def __init__(self):
        super(CheckBoxTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("pushButton");

        layout = QHBoxLayout();
        self.setLayout(layout)

        btn1 = QCheckBox('按钮1');
        btn2 = QCheckBox('按钮2');
        btn3 = QCheckBox('按钮3');
        btn4 = QCheckBox('按钮4');

        btn1.setChecked(True);
        # 启用三态
        btn3.setTristate(True);
        # 设为半选中
        btn3.setCheckState(Qt.PartiallyChecked);

        btn1.stateChanged.connect(self.toggle);
        btn2.stateChanged.connect(self.toggle);
        btn3.stateChanged.connect(self.toggle);
        btn4.stateChanged.connect(self.toggle);
        # btn1.toggled.connect(self.toggle);
        # btn2.toggled.connect(self.toggle);
        # btn3.toggled.connect(self.toggle);
        # btn4.toggled.connect(self.toggle);

        self.btn1 = btn1;
        self.btn2 = btn2;
        self.btn3 = btn3;
        self.btn4 = btn4
        layout.addWidget(btn1);
        layout.addWidget(btn2);
        layout.addWidget(btn3);
        layout.addWidget(btn4);

    def toggle(self):
        temp=self.sender();
        if isinstance(temp,QCheckBox):
            item: QCheckBox=temp;
            print(item.text(),item.isChecked(),item.checkState());
        else:
            print(type(temp));




if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = CheckBoxTest();
    main.show();
    sys.exit(app.exec_());
