from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton

def set_palette_style(widget):
    # 创建一个 QPalette 对象
    palette = QPalette()

    # 设置窗口背景色为灰色
    palette.setColor(QPalette.Window, Qt.gray)

    # 设置文本颜色为黑色
    palette.setColor(QPalette.Text, Qt.black)

    # 设置按钮的背景色为蓝色
    palette.setColor(QPalette.Button, Qt.blue)

    # 将 QPalette 应用到窗口
    widget.setPalette(palette)

if __name__ == '__main__':
    # 创建一个 QApplication 对象
    app = QApplication([])

    # 创建一个 QWidget 作为主窗口
    window = QWidget()

    # 创建一个垂直布局
    layout = QVBoxLayout()

    # 创建一个按钮，并设置文本为'点击我'
    button = QPushButton('点击我')
    layout.addWidget(button)

    # 设置窗口的布局为垂直布局
    window.setLayout(layout)

    # 设置窗口的标题
    window.setWindowTitle('QPalette 示例')

    # 使用 set_palette_style 函数设置窗口样式
    set_palette_style(window)

    # 显示窗口
    window.show()

    # 进入主事件循环，等待用户交互
    app.exec_()