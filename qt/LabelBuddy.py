import sys

from PyQt5.QtWidgets import QWidget, QApplication, QPushButton, QLabel, QLineEdit, \
    QGridLayout


class LabelBuddy(QWidget):
    def __init__(self):
        super().__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("伙伴关系");

        labelName = QLabel('&Name',self);
        lineEditName=QLineEdit(self);
        labelName.setBuddy(lineEditName);

        labelPW = QLabel('&Password',self);
        lineEditPW=QLineEdit(self);
        labelPW.setBuddy(lineEditPW);

        btnOk=QPushButton('&Ok');
        btnCancel=QPushButton('&Cancel');

        layout=QGridLayout(self);

        layout.addWidget(labelName,0,0);
        layout.addWidget(lineEditName,0,1,1,2);
        layout.addWidget(labelPW,1,0);
        layout.addWidget(lineEditPW,1,1,1,2);
        layout.addWidget(btnOk,2,1)
        layout.addWidget(btnCancel, 2, 2)

        self.setLayout(layout);


    def linkHovered(self):
        print('鼠标滑过');

    def linkClicked(self):
        print('单击');


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = LabelBuddy();
    main.show();
    sys.exit(app.exec_());
