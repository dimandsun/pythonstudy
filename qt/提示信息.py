import sys

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QToolTip, QApplication, QPushButton, QHBoxLayout


class ToolTipForm(QWidget):
    def __init__(self):
        super().__init__();
        self.initUI();

    def initUI(self):
        QToolTip.setFont(QFont('SansSerif', 12));
        self.setToolTip('鼠标放上去会显示的文本,要等1秒钟');
        self.setWindowTitle("设置控件提示消息")
        self.setGeometry(400,500,300,300)
        self.button=QPushButton('按钮');
        self.button.setToolTip("鼠标放上去的提示");
        layout=QHBoxLayout();
        layout.addWidget(self.button);
        self.setLayout(layout)


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = ToolTipForm();
    main.show();
    sys.exit(app.exec_());
