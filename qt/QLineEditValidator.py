import sys

from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit


class LineEdit(QWidget):
    """
    normal
    NoEcho 输入文本，不回显
    Password
    PasswordEchoOnEdit
    """
    def __init__(self):
        super(LineEdit,self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("校验器");

        layout=QFormLayout();
        lineInt=QLineEdit();
        lineDouble=QLineEdit();
        lineVali=QLineEdit();

        layout.addRow("整数",lineInt);
        layout.addRow("double",lineDouble);
        layout.addRow("数字和字母",lineVali);

        lineInt.setPlaceholderText("整数");
        lineDouble.setPlaceholderText("double");
        lineVali.setPlaceholderText("数字和字母");

        intVa=QIntValidator(self);
        intVa.setRange(1,99);

        dobleVa=QDoubleValidator(self);
        dobleVa.setRange(-360,360);
        # 浮点数显示样式
        dobleVa.setNotation(QDoubleValidator.StandardNotation);
        # 设置浮点数精度
        dobleVa.setDecimals(2);

        # 字母和数字
        reg=QRegExp("[a-zA-Z0-9]+$");
        v=QRegExpValidator(self);
        v.setRegExp(reg);

        lineInt.setValidator(intVa);
        lineDouble.setValidator(dobleVa);
        lineVali.setValidator(v);

        self.setLayout(layout)


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = LineEdit();
    main.show();
    sys.exit(app.exec_());
