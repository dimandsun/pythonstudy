import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont, QIcon
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit, QDialog, QVBoxLayout, QPushButton


class PushButtonTest(QDialog):
    """
    """

    def __init__(self):
        super(PushButtonTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("pushButton");

        layout = QVBoxLayout();
        self.setLayout(layout)

        btn1 = QPushButton('按钮1');
        # 启用选中与不选中
        btn1.setCheckable(True);
        # 切换状态
        btn1.toggle();

        # 可以对一个事件源绑定多个监听事件
        btn1.clicked.connect(lambda: self.click(btn1));
        btn1.clicked.connect(self.click_btn1);

        btn2 = QPushButton('带图标');
        btn2.setIcon(QIcon("../images/cat.ico"))
        btn2.clicked.connect(lambda: btn1.toggle());

        btn3 = QPushButton('不可用')
        btn3.setEnabled(False);

        btn4 = QPushButton('&M默认按钮');
        # 光标自动定位该按钮
        btn4.setDefault(True);
        btn4.clicked.connect(lambda: self.click(btn4));

        self.btn1 = btn1;
        self.btn2 = btn2;
        self.btn3 = btn3;
        self.btn4 = btn4
        layout.addWidget(btn1);
        layout.addWidget(btn2);
        layout.addWidget(btn3);
        layout.addWidget(btn4);

    def click_btn1(self):
        print("单击按钮1", end='');
        if self.btn1.isChecked():
            print("被选中");
        else:
            print("未被选中")

    def click(self, btn):
        print('单击的按钮是:' + btn.text());


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = PushButtonTest();
    main.show();
    sys.exit(app.exec_());
