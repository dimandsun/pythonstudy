import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit, QTextEdit, QVBoxLayout


class TextEditTest(QWidget):
    """
    """
    def __init__(self):
        super(TextEditTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("校验器");

        layout=QVBoxLayout();
        self.setLayout(layout)

        edit1=QTextEdit();
        edit1.setHtml("")

        layout.addWidget(edit1);

    def textChange(self,text):
        print(text)
    def textFinish(self):
        print('文本结束输入')


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = TextEditTest();
    main.show();
    sys.exit(app.exec_());
