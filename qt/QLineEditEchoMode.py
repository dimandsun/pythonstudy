import sys

from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit


class LineEdit(QWidget):
    """
    normal
    NoEcho 输入文本，不回显
    Password
    PasswordEchoOnEdit
    """
    def __init__(self):
        super().__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("文件输入框的回显");

        layout=QFormLayout();
        normal=QLineEdit();
        noEcho=QLineEdit();
        password=QLineEdit();
        passwordEchoOnEdit=QLineEdit();

        layout.addRow("Normal",normal);
        layout.addRow("NoEcho",noEcho);
        layout.addRow("Password",password);
        layout.addRow("PasswordEchoOnEdit",passwordEchoOnEdit);

        normal.setPlaceholderText("normal");
        noEcho.setPlaceholderText("不回显");
        password.setPlaceholderText("输入密码");
        passwordEchoOnEdit.setPlaceholderText("AA");

        normal.setEchoMode(QLineEdit.Normal);
        noEcho.setEchoMode(QLineEdit.NoEcho);
        password.setEchoMode(QLineEdit.Password);
        passwordEchoOnEdit.setEchoMode(QLineEdit.PasswordEchoOnEdit);

        self.setLayout(layout)


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = LineEdit();
    main.show();
    sys.exit(app.exec_());
