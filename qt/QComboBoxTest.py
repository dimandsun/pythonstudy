import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont, QIcon
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit, QDialog, QVBoxLayout, QPushButton, \
    QRadioButton, QHBoxLayout, QCheckBox, QComboBox, QLabel


class ComboBoxTest(QWidget):
    """
    下拉选择框，单选

    """

    def __init__(self):
        super(ComboBoxTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("下拉选择框");

        layout = QVBoxLayout();
        self.setLayout(layout)

        self.label=QLabel('选择语言');
        box=QComboBox();
        box.addItem("Java");
        box.addItems(["C","C++","Python"]);
        box.currentIndexChanged.connect(self.change)

        self.box = box;
        layout.addWidget(self.label);
        layout.addWidget(box);

        for i in range(self.box.count()):
            print(i,self.box.itemText(i));

    def change(self,item):
        print(item);
        self.label.setText(self.box.currentText())
        self.label.adjustSize();


if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = ComboBoxTest();
    main.show();
    sys.exit(app.exec_());
