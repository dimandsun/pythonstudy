import sys

from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont, QIcon
from PyQt5.QtWidgets import QWidget, QApplication, QFormLayout, QLineEdit, QDialog, QVBoxLayout, QPushButton, \
    QRadioButton, QHBoxLayout


class RadioButtonTest(QWidget):
    """
    """

    def __init__(self):
        super(RadioButtonTest, self).__init__();
        self.initUI();

    def initUI(self):
        self.setWindowTitle("pushButton");

        layout = QHBoxLayout();
        self.setLayout(layout)

        btn1 = QRadioButton('按钮1');
        btn1.setChecked(True);
        btn2 = QRadioButton('按钮2');


        btn3 = QRadioButton('按钮3');
        btn4 = QRadioButton('按钮4');
        btn1.toggled.connect(self.toggle);
        btn2.toggled.connect(self.toggle);
        btn3.toggled.connect(self.toggle);
        btn4.toggled.connect(self.toggle);

        self.btn1 = btn1;
        self.btn2 = btn2;
        self.btn3 = btn3;
        self.btn4 = btn4
        layout.addWidget(btn1);
        layout.addWidget(btn2);
        layout.addWidget(btn3);
        layout.addWidget(btn4);

    def toggle(self):
        temp=self.sender();
        if isinstance(temp,QRadioButton):
            item: QRadioButton=temp;
            print(item.text(),item.isChecked());
        else:
            print(type(temp));




if __name__ == "__main__":
    app = QApplication(sys.argv);
    main = RadioButtonTest();
    main.show();
    sys.exit(app.exec_());
