import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QDesktopWidget, QLabel, QPushButton, QGraphicsScene, \
    QGraphicsView
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter
import const
from SnakeGame import SnakeGame


class SnakeWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # 设置窗口标题和大小
        self.setWindowTitle('超级贪吃蛇')
        self.setFixedSize(const.WINDOW_WIDTH, const.WINDOW_HEIGHT)

        # 设置窗口居中显示
        screen_rect = QDesktopWidget().availableGeometry(self)
        window_rect = self.frameGeometry()
        window_rect.moveCenter(screen_rect.center())
        self.move(window_rect.topLeft())

    def start_game(self):
        # 创建SnakeGame对象并开始游戏循环
        game = SnakeGame()
        game.start()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = SnakeWindow()
    window.show()
    sys.exit(app.exec_())
