table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 8637678}
print('Jack: {0[Jack]:d}; Sjoerd: {0[Sjoerd]:d}; Dcab: {0[Dcab]:d}'.format(table))
print('Jack: {Jack:d}; Sjoerd: {Sjoerd:d}; Dcab: {Dcab:d}'.format(**table))
print('Jack: {0[Jack]}; Sjoerd: {0[Sjoerd]}; Dcab: {0[Dcab]}'.format(table))
print(f'Jack: {table["Jack"]}; Sjoerd: {table["Sjoerd"]}; Dcab: {table["Dcab"]}')
print(table)
print(vars(table))