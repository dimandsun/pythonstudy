import codecs

print('hello \nworld');  #
# 字符串不转义
print(r'hello \n world');
# 多行字符串等价于java中的""" """
print('''
    你好
''');

range(2,5)
# string-f 格式化字符串 F或者f都可以
for i in range(5): print(f'第{i + 1}个数是{i}');
for i in range(5): print(F'第{i + 1}个数是{i}');

# string-u unicode字符串
text = u'unicode字符串';
print(text);

# string-b 字节字符串
text=b'12334asd';
print(f"string-b:{text}");

a='a' 'b';
print(a);



