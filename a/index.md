安装
pip install fastapi
pip install python-dotenv
pip install "uvicorn[standard]"

python-dotenv:
加载环境变量参考env.py

fastapi 
uvicorn/Hypercorn: 支持http2和websocket
sanic: 路由、中间件、orm
starlette: 支持http2和websocket、中间件、路由、异常处理

启动fastapi
cmd到main.py所在目录，E:\wo\python 执行 uvicorn main:app --reload

ip:port/docs 查看文档
ip:port/redoc 查看接口文档

a**n :表示a的n次方
字符串前加r'不转义的字符串\''
"""这是文本块"""
非字符串不能与字符串拼接 3+'3'会报错
字符串重复 n*str 表示str重复n次
相邻字符串字面量合并 'a' 'b'->'ab'

字符串支持 索引 （下标访问），第一个字符的索引是 0。单字符没有专用的类型，就是长度为一的字符串
索引还支持负数，用负数索引时，从右边开始计数：注意，-0 和 0 一样，因此，负数索引从 -1 开始
字符串切片 word[0:2]
切片索引的默认值很有用；省略开始索引时，默认值为 0，省略结束索引时，默认为到字符串的结尾：

while 条件表达式:
    执行语句

if 条件表达式:
    执行语句
elif 条件表达式:
    执行语句
else :
    执行语句

for w in ['a','b',1,2]:
    语句

users = {'Hans': 'active', 'Éléonore': 'inactive', '景太郎': 'active'}

很难正确地在迭代多项集的同时修改多项集的内容。更简单的方法是迭代多项集的副本或者创建新的多项集：
for user, status in users.copy().items():
    if status == 'inactive':
        del users[user]

active_users = {}
for user, status in users.items():
    if status == 'active':
        active_users[user] = status

0到5
for i in range(5):

5到9的集合
list(range(5, 10))

等差数列 从0开始，步长3，最后索引的绝对值小于10
list(range(0, 10, 3))-》[0, 3, 6, 9]
list(range(-10, -100, -30))-》[-10, -40, -70]

seasons = ['Spring', 'Summer', 'Fall', 'Winter']
list(enumerate(seasons))
-》[(0, 'Spring'), (1, 'Summer'), (2, 'Fall'), (3, 'Winter')]
list(enumerate(seasons, start=1))
-》[(1, 'Spring'), (2, 'Summer'), (3, 'Fall'), (4, 'Winter')]

for或while可以加else
break结束的不会执行else分支
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'equals', x, '*', n//x)
            break
    else:
        # loop fell through without finding a factor
        print(n, 'is a prime number')

2 is a prime number
3 is a prime number
4 equals 2 * 2
5 is a prime number
6 equals 2 * 3
7 is a prime number
8 equals 2 * 4
9 equals 3 * 3

else 子句用于循环时比起 if 语句的 else 子句，更像 try 语句的。try 语句的 else 子句在未发生异常时执行，循环的 else 子句则在未发生 break 时执行。 try 语句和异常详见 异常的处理。

整除：//

pass 语句不执行任何动作。语法上需要一个语句，但程序毋需执行任何动作时，可以使用该语句。例如：
class MyEmptyClass:
    pass

pass 还可用作函数或条件语句体的占位符，让你保持在更抽象的层次进行思考。pass 会被默默地忽略：
def initlog(*args):
    pass   # Remember to implement this!

match语句
def http_error(status):
    match status:
        case 400:
            return "Bad request"
        case 404:
            return "Not found"
        case 418:
            return "I'm a teapot"
        case 401 | 403 | 404:
            return "Not allowed"
        case _:
            return "Something's wrong with the internet"

# point is an (x, y) tuple
match point:
    case (0, 0):
        print("Origin")
    case (0, y):
        print(f"Y={y}")
    case (x, 0):
        print(f"X={x}")
    case (x, y):
        print(f"X={x}, Y={y}")
    case _:
        raise ValueError("Not a point")
请仔细学习此代码！第一个模式有两个字面值，可视为前述字面值模式的扩展。接下来的两个模式结合了一个字面值和一个变量，变量 绑定 了来自主语（point）的一个值。第四个模式捕获了两个值，使其在概念上与解包赋值 (x, y) = point 类似。
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def where_is(point):
    match point:
        case Point(x=0, y=0):
            print("Origin")
        case Point(x=0, y=y):
            print(f"Y={y}")
        case Point(x=x, y=0):
            print(f"X={x}")
        case Point():
            print("Somewhere else")
        case _:
            print("Not a point")

斐波那契数列函数
def fib(n):    # write Fibonacci series up to n
    """Print a Fibonacci series up to n."""
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()

# Now call the function we just defined:
fib(2000)

def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)
只给出必选实参：ask_ok('Do you really want to quit?')

给出一个可选实参：ask_ok('OK to overwrite the file?', 2)

给出所有实参：ask_ok('OK to overwrite the file?', 2, 'Come on, only yes or no!')


函数参数的默认值是在函数定义时被计算的，不是在函数调用时计算的
i = 5
def f(arg=i):
    print(arg)
i = 6
f()
输出5，而不是6

def f(a, L=[]):
    L.append(a)
    return L
print(f(1))
print(f(2))
print(f(3))
-》
[1]
[1, 2]
[1, 2, 3]

def cheeseshop(kind, *arguments, **keywords):
    pass
接受一个参数 kind，以及一个或多个参数 arguments 和一个或多个关键字参数 keywords。

def f(pos1, pos2, /, pos_or_kwd, *, kwd1, kwd2):
    pass
该函数接受四个参数：pos1、pos2、pos_or_kwd 和 kwd1、kwd2。
/ 表示 pos_or_kwd 可以作为位置参数或关键字参数传入
* 表示 kwd1 和 kwd2 是关键字参数，必须以关键字的形式传入。
/ 和 * 是可选的。这些符号表明形参如何把参数值传递给函数：位置、位置或关键字、关键字。关键字形参也叫作命名形参。
默认情况下，参数可以按位置或显式关键字传递给 Python 函数。为了让代码易读、高效，最好限制参数的传递方式，这样，开发者只需查看函数定义，即可确定参数项是仅按位置、按位置或关键字，还是仅按关键字传递。
仅限位置形参应放在 / （正斜杠）前。/ 用于在逻辑上分割仅限位置形参与其它形参
/ 后可以是 位置或关键字 或 仅限关键字 形参。


lambda:
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[0])
print(pairs)

操作文件的mode ： r w r+ a 默认r
读写二进制文件时，要要使用二进制模式，因为其他模式会自动转换换行符，导致二进制文件读取数据错乱。