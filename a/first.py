import sys
# 当代码行的结尾没有加分号时，Python 会自动换行并在下一行继续执行代码。这是 Python 的默认行为
# 当代码行的结尾加分号时，Python 会将该代码行视为一个完整的语句，并立即执行它。加分号的主要作用是将多个语句写在一行上，提高代码的可读性和简洁性
print("encode:"+sys.getdefaultencoding())
print("你好，python");
a=1;
print(a);
