text='asd好几口';
print(text);

# 将字符串转换成指定的字节序列，输出 b'asd\xe5\xa5\xbd\xe5\x87\xa0\xe5\x8f\xa3'
textByte=text.encode("utf-8");
print(textByte);
# 将字节序列转成字符串
text=textByte.decode("utf-8");
print(text);


#有不能转换的编码时会报错
# print(text.encode('ascii'));
# 输出b'asd'
text1=text.encode("ascii",errors='ignore');
print(text1);
# 输出 b'asd???'
print(text.encode('ascii',errors='replace'));