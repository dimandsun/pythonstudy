import json

a={
    "name":"123"
}
print(a);
print(type(a));
# python对象转换成json对象
json_str=json.dumps(a);
print(json_str);
print(type(json_str));
aa=json.loads(json_str);
print(f'{type(aa)}{a}')
# print(aa.name)

# json格式数据写入文件
with open('json.json','w') as f:
    json.dump(a,f);
