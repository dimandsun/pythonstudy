"""
MkDocs：轻量级的自动化文档工具,使用md语法来编写文档，并将文档生成静态网页
Sphinx 支持多种格式的文档，使用更麻烦一点，好处是文档质量更高
这是模块注释，通常用来说明该python模块
"""
import math


class Car:
    """
    类属性文档化
    color: 颜色
    """
    def __init__(self, color, brand, speed):
        self.color = color;
        self.brand = brand;
        self.speed = speed;


def sqrt(num):
    """
    这是函数注释,这个函数用于求平方根
    :param num:
    :return:
    """
    return math.sqrt(num);


def add(a, b):
    """
    这是函数注释
    :param a:
    :param b:
    :return:
    """
    return a + b;
