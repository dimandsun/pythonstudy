
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[0])
print(pairs)

squares0 = []
for x in range(10):
    squares0.append(x**2)
squares1 = list(map(lambda x: x ** 2, range(10)))
squares2 = [x ** 2 for x in range(10)]
# [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

combs0=[(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
combs1 = []
for x in [1,2,3]:
    for y in [3,1,4]:
        if x != y:
            combs1.append((x, y))
# ->[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

vec = [[1,2,3], [4,5,6], [7,8,9]]
result=[num for elem in vec for num in elem]
# [1, 2, 3, 4, 5, 6, 7, 8, 9]

matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
]
transposed1=[[row[i] for row in matrix] for i in range(4)]

transposed2 = []
for i in range(4):
    transposed2.append([row[i] for row in matrix])

transposed3 = []
for i in range(4):
    # the following 3 lines implement the nested listcomp
    transposed_row = []
    for row in matrix:
        transposed_row.append(row[i])
    transposed3.append(transposed_row)

transposed4=list(zip(*matrix))
# [[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

