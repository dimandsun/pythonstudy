"""
变量类型注解 变量: 变量类型

返回值类型注解 -> 返回值类型

类型检查不会在运行时强制检查类型
"""


def 接收字符串返回字符串(text: str) -> str:
    """
    这个方法接收一个字符串参数text，返回一个字符串. 但是只是增加提示，实际上参数不是字符串也不会报错。
    :param text:
    :return:
    """
    return f"你输入的是:{text}";


print(接收字符串返回字符串(213));

from typing import List


class Person:
    def __init__(self, name: str, age: int):
        self.name = name;
        self.age = age;

    def getList(self) -> List[int]:
        return [12, 2, 3];


person = Person('小明', 12);
print(person.name + f"今年{person.age}岁")
print(person.name + f"的list:{person.getList()}岁")
